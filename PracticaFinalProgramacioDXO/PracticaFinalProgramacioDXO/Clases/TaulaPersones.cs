﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{
    public class TaulaPersones : IEnumerable<Persona>
    {
        Dictionary<string, Persona> taula;
        /// <summary> 
        /// Crea una taula de referències a persones 
        /// </summary> 
        public TaulaPersones()
        {
            taula = new Dictionary<string, Persona>();
        }

        /// <summary> 
        /// Assigna o obté una persona de la taula 
        /// </summary> 
        public Persona this[string nom]
        {
            get { return taula[nom]; }
            set { taula[nom] = value; }
        }

        /// <summary> 
        /// Obtè el número total de persones 
        /// </summary> 
        public int NumPersones
        {
            get { return taula.Count; }
        }

        /// <summary> 
        /// Afegeix una persona a la taula 
        /// </su/mmary> 
        /// <param name="conv">Convidat a afegir</param> 
        public void Afegir(Persona pers)
        {
            if (!taula.Keys.Contains(pers.Nom))
                taula.Add(pers.Nom, pers);
        }

        /// <summary> 
        /// Eliminar una persona de la taula 
        /// </summary> 
        /// <param name="conv">Convidat a eliminar</param> 
        public void Eliminar(Persona pers)
        {
            taula.Remove(pers.Nom);
        }

        /// <summary> 
        /// Elimina la persona donat el seu nom 
        /// </summary> 
        /// <param name="posicio">Posició a eliminar</param> 
        public void Eliminar(string nom)
        {
            taula.Remove(nom);
        }
        
        public IEnumerator<Persona> GetEnumerator()
        {
            foreach  (KeyValuePair<string, Persona> p in taula)
            {
                yield return p.Value;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}