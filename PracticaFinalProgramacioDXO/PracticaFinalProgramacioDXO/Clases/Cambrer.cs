﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{

    public class Cambrer : Persona
    {
        private string nom;
        private static int nCambrer;

        /// <summary> 
        /// Crea un cambrer (Persona de la que no importa el nom, i es dirà "Cambrer1, Cambrer2...") 
        /// </summary> 
        public Cambrer(string nom)
            : base(nom)
        {
            this.nom = "Cambrer" + nCambrer;
            nCambrer++;
        }

        /// <summary> 
        /// Interès del cambrer per una posició 
        /// </summary> 
        /// <param name="pos">Posició per la que s'interessa</param> 
        /// <returns>Retorna 0 si no hi ha ningú, 1 si hi ha un convidat i -1 si hi ha un cambrer</returns> 
        public override int Interes(Posicio pos)
        {
            if (pos.Buida) return 0;

            else if (((Persona)pos).EsConvidat()) return 1;

            else return -1;
        }

        /// <summary> 
        /// Retorna que el Cambrer no és un convidat 
        /// </summary> 
        /// <returns>false</returns> 
        public override bool EsConvidat()
        {
            return false;
        }

        /// <summary>
        /// Retorna el nom.
        /// </summary>
        /// <returns>Nom</returns>
        public override string ToString()
        {
            return this.nom;
        }
    }
}



