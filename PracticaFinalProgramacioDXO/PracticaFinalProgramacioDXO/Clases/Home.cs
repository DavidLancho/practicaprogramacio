﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{
    public class Home : Convidat
    {
        private string nom;
        private int sexe;

        /// <summary>
        /// Crea un Home
        /// </summary>
        /// <param name="nom">String que l'identificarà</param>
        /// <param name="sexe">Plus de simpatia envers del sexe contrari</param>
        public Home(string nom, int sexe, TaulaPersones taula)
            : base(nom, sexe, taula)
        {
            this.nom = nom;
            this.sexe = sexe;
        }

        /// <summary>
        /// Interès d'aquest home per una posició
        /// </summary>
        /// <param name="pos">Posició per la qual s'interessa</param>
        /// <returns>Interès quantificat</returns>
        public override int Interes(Posicio pos)
        {
            if (!pos.Buida)
            {
                if (!((Persona)pos).EsConvidat())
                {
                    return 1;
                }
                else if (((Convidat)pos).EsHome())
                {
                    return this[(Persona)pos];
                }
                else
                {
                    return this[(Persona)pos] + PlusSexe;
                }
            }
            else return 0;
        }

        /// <summary>
        /// Retorna si és Home.
        /// </summary>
        /// <returns>Cert</returns>
        public override bool EsHome()
        {
            return true;
        }

        /// <summary>
        /// Retorna el nom.
        /// </summary>
        /// <returns>Nom</returns>
        public override string ToString()
        {
            return this.nom;
        }
    }
}
