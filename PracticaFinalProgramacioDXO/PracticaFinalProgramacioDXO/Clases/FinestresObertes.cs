﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{
    public class Finestres : IEnumerable<wndTaulaSimpaties>
    {
        Dictionary<Persona, wndTaulaSimpaties> taula;
        /// <summary> 
        /// Crea una taula de referències a persones 
        /// </summary> 
        public Finestres()
        {
            taula = new Dictionary<Persona, wndTaulaSimpaties>();
        }

        /// <summary> 
        /// Assigna o obté una persona de la taula 
        /// </summary> 
        public wndTaulaSimpaties this[Persona per]
        {
            get { return taula[per]; }
            set { taula[per] = value; }
        }

        /// <summary> 
        /// Obtè el número total de persones 
        /// </summary> 
        public int NumFinestres
        {
            get { return taula.Count; }
        }

        /// <summary> 
        /// Afegeix una persona a la taula 
        /// </su/mmary> 
        /// <param name="conv">Convidat a afegir</param> 
        public void Afegir(wndTaulaSimpaties frm, Persona per)
        {
            if (!taula.Keys.Contains(per))
                taula.Add(per, frm);
        }

        /// <summary> 
        /// Eliminar una persona de la taula 
        /// </summary> 
        /// <param name="pers">Persona</param> 
        public void Eliminar(Persona per)
        {
            taula.Remove(per);
        }
        public IEnumerator<wndTaulaSimpaties> GetEnumerator()
        {
            foreach (KeyValuePair<Persona, wndTaulaSimpaties> p in taula)
            {
                yield return p.Value;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}