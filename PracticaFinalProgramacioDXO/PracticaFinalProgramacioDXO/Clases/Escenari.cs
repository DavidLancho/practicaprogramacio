﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{
    public class Escenari
    {
        Posicio[,] pos;
        int files;
        int columnes;
        int nHomes;
        int nDones;
        int nCambrers;
        public Random r = new Random();
        TaulaPersones taula;

        /// <summary> 
        /// Crea un escenari donades unes mides 
        /// </summary> 
        /// <param name="files">Número de files de l'escenari</param> 
        /// <param name="columnes">Número de columnes de l'escenari</param> 
        public Escenari(int files, int columnes)
        {
            this.files = files;
            this.columnes = columnes;
            pos = new Posicio[files, columnes];
            taula = new TaulaPersones();
            for (int i = 0; i < files; i++)
            {
                for (int j = 0; j < columnes; j++)
                {
                    this[i, j] = new Posicio(i, j);
                }
            }
        }

        /// <summary> 
        /// Retorna el número de files de l'escenari 
        /// </summary> 
        public int Files
        {
            get { return files; }
        }

        /// <summary> 
        /// Retorna el número de columnes de l'escenari 
        /// </summary> 
        public int Columnes
        {
            get { return columnes; }
        }

        /// <summary> 
        /// Retorna el número de homes que hi ha dins de l'escenari 
        /// </summary> 
        public int Homes
        {
            get { return nHomes; }
        }

        /// <summary> 
        /// Retorna el número de dones que hi ha dins de l'escenari 
        /// </summary> 
        public int Dones
        {
            get { return nDones; }
        }

        /// <summary> 
        /// Retorna el número de Cambrers que hi ha dins de l'escenari 
        /// </summary> 
        public int Cambrers
        {
            get { return nCambrers; }
        }

        /// <summary> 
        /// Obtè la TaulaPersones que guarda l'escenari 
        /// </summary> 
        public TaulaPersones Taula
        {
            get { return taula; }
        }

        /// <summary> 
        /// Mou una persona de (filOrig, colOrig) fins a la posicio adjacent (filDesti,colDesti) 
        /// Es suposa que les coordenades són vàlides, que hi ha una persona a l'origen i que 
        /// el destí està buit. 
        /// </summary> 
        /// <param name="filOrig">Fila de la coordenada d'origen</param> 
        /// <param name="colOrig">Columna de la coordenada d'origen</param> 
        /// <param name="filDesti">Fila de la coordenada de destí</param> 
        /// <param name="colDesti">Columna de la coordenada de destí</param> 
        private void Moure(int filOrig, int colOrig, int filDesti, int colDesti)
        {
            if (!this[filOrig, colOrig].Buida)
            {
                this[filOrig, colOrig].Columna = colDesti;
                this[filOrig, colOrig].Fila = filDesti;
                this[filDesti, colDesti] = this[filOrig, colOrig];
                this[filOrig, colOrig] = new Posicio(filOrig, colOrig);
            }
        }

        /// <summary> 
        /// Retorna la Posició que hi ha en una coordenada donada 
        /// </summary> 
        public Posicio this[int fila, int col]
        {
            get
            {
                return pos[fila, col];
            }
            set
            {
                pos[fila, col] = value;
            }
        }

        /// <summary> 
        /// Mira si una coordenada està fora del tauler, o ocupada per una persona. 
        /// </summary> 
        /// <param name="fil">fila de la coordenada</param> 
        /// <param name="col">columna de la coordenada</param> 
        /// <returns>retorna si la coordenada és vàlida i està buida</returns> 
        public bool DestiValid(int fil, int col)
        {
            if (Files <= fil || fil < 0)
                return false;

            else if (Columnes <= col || col < 0)
                return false;

            else
            {
                if (this[fil, col] == null || this[fil, col].Buida) return true;
                else return false;
            }
        }

        /// <summary> 
        /// Retorna el contingut del escenari en forma de matriu d'strings, 
        /// representant cada persona amb el seu nom 
        /// </summary> 
        /// <returns>Matriu de caràcters</returns> 
        public String[,] ContingutNoms()
        {
            String[,] llista = new String[files, columnes];
            IEnumerator<Persona> i = taula.GetEnumerator();
            while (i.MoveNext())
            {
                Persona p = i.Current;
                llista[p.Fila, p.Columna] = p.Nom;
            }
            return llista;
        }

        /// <summary> 
        /// Elimina una persona de l'escenari i de la taula de persones 
        /// </summary> 
        /// <param name="fil">Fila on està la persona</param> 
        /// <param name="col">Columna on està la persona</param> 
        public void Buidar(int fil, int col)
        {
            if (!this[fil, col].Buida)
            {
                this[fil, col] = new Posicio(fil, col);
            }
        }

        /// <summary> 
        /// Posa una Persona dins de l'escenari i a la taula de persones 
        /// Si la posició de la persona ja està ocupada, genera una excepció 
        /// </summary> 
        /// <param name="pers">Persona a afegir</param> 
        public void Posar(Persona pers)
        {
            bool fet = false;
            int fil;
            int colum;
            while (!fet)
            {
                fil = r.Next(Files);
                colum = r.Next(Files);
                if (this[fil, colum].Buida)
                {
                    pers.Fila = fil;
                    pers.Columna = colum;
                    taula.Afegir(pers);
                    this[fil, colum] = pers;

                    if (!pers.EsConvidat())
                    {
                        nCambrers++;
                    }

                    else if (((Convidat)pers).EsHome())
                    {
                        nHomes++;
                    }

                    else
                    {
                        nDones++;
                    }
                    fet = true;
                }


            }
        }

        /// <summary> 
        /// Mira si en el tauler hi ha alguna persona amb aquest nom 
        /// </summary> 
        /// <param name="nom">Nom a cercar</param> 
        /// <returns>Si hi ha coincidència</returns> 
        public bool NomRepetit(string nom)
        {
            return true;
        }

        /// <summary> 
        /// Fa que totes les persones facin un moviment 
        /// </summary>
        public void Cicle()
        {
            IEnumerator<Persona> i = taula.GetEnumerator();

            while (i.MoveNext())
            {
                Persona p = i.Current;
                Direccio resultat = p.OnVaig(this);
                if (Direccio.Amunt == resultat)
                {
                    this.Moure(p.Fila, p.Columna, p.Fila- 1, p.Columna );

                }
                else if (Direccio.Avall == resultat)
                {
                    this.Moure(p.Fila, p.Columna, p.Fila+ 1, p.Columna );
                }
                else if (Direccio.Dreta == resultat)
                {
                    this.Moure(p.Fila, p.Columna, p.Fila , p.Columna+ 1);
                }
                else if (Direccio.Esquerra == resultat)
                {
                    this.Moure(p.Fila, p.Columna, p.Fila , p.Columna- 1);
                }
            }

        }
    }
}
        

