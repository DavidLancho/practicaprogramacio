﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{
    public enum Direccio { Quiet, Amunt, Dreta, Avall, Esquerra };
    public abstract class Persona : Posicio
    {
        private string nom;
        /// <summary> 
        /// Crea una persona 
        /// </summary> 
        /// <param name="nom">String que identifica la persona</param> 
        /// <param name="fil">Fila on està localitzada</param> 
        /// <param name="col">Columna on està localitzada</param> public
        Persona(int fil, int col , string nom) : base(fil, col)
        {
            this.nom = nom;
            
        }

        /// <summary> 
        /// Crea una persona 
        /// </summary> 
        /// <param name="nom">nom de la persona</param> 
        public Persona(string nom)
        {
            this.nom = nom;
        }

        /// <summary> 
        /// Crea una persona 
        /// </summary>
        public Persona() : base()
        {
            this.nom = "";
        }

        /// <summary> 
        /// Obté el nom de la persona 
        /// </summary> 
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        /// <summary> 
        /// Retorna que la posició ocupada per aquesta persona no està buida 
        /// </summary> 
        public override bool Buida
        {
            get
            {
                return false;
            }
        }

        /// <summary> 
        /// Atraccio de persona sobre una determinada posicio 
        /// </summary> 
        /// <param name="fil">Fila de la posició</param> 
        /// <param name="col">Columan de la posició</param> 
        /// <param name="esc">Escenari on estan situats</param> 
        /// <returns>Atracció quantificada</returns> 
        private double Atraccio(int fil, int col, Escenari esc)
        {
            double suma = 0;
            IEnumerator<Persona> i = esc.Taula.GetEnumerator();

            while (i.MoveNext())
            {
                Persona p = i.Current;
                if (p.nom != nom) suma += Interes(esc[p.Fila, p.Columna]) / Distancia(new Posicio(fil, col), (new Posicio(p.Fila, p.Columna)));
            }

            return suma;
        }

        /// <summary> 
        /// Decideix quin serà el següent moviment que farà la persona 
        /// </summary> 
        /// <param name="esc">Escenari on esta situada la persona</param> 
        /// <returns>Una de les 5 possibles direccions (Quiet, Amunt, Avall, Dreta, Esqu erra</returns>
        public Direccio OnVaig(Escenari esc)
        {
            double quiet = Atraccio(Fila, Columna, esc);
            double avall = double.MinValue;
            double amunt = double.MinValue;
            double esquerra = double.MinValue;
            double dreta = double.MinValue;
            if (esc.DestiValid(this.Fila + 1, this.Columna))
            {
                avall = Atraccio(Fila +1, Columna, esc);
            }
            if (esc.DestiValid(this.Fila-1, this.Columna))
            {
                 amunt = Atraccio(Fila-1, Columna  , esc);
            }
            if (esc.DestiValid(this.Fila, this.Columna-1))
            {
                 esquerra = Atraccio(Fila , Columna- 1, esc);
            }
            if (esc.DestiValid(this.Fila, this.Columna+1))
            {
                 dreta = Atraccio(Fila , Columna+ 1, esc);
            }
            List<Direccio> llista = new List<Direccio>();
            llista.Add(Direccio.Quiet);
            double max=quiet;
            if (max <= dreta)
            {
                    if (max < dreta) llista.Clear();
                    llista.Add(Direccio.Dreta);
                    max = dreta;
            }
            if (max <= esquerra)
            {
                if (max < esquerra) llista.Clear();
                    llista.Add(Direccio.Esquerra);
                    max = esquerra;
                
            }
            if (max <= amunt)
            {
                if (max < amunt) llista.Clear();
                llista.Add(Direccio.Amunt);
                max = amunt;
            }
            if (max <= avall)
            {
                if (max < avall) llista.Clear();
                llista.Add(Direccio.Avall);
                max = avall;
            }
            return llista[esc.r.Next(llista.Count)];
        }

        /// <summary> 
        /// Interès de la persona sobre una determinada posició 
        /// </summary> 
        /// <param name="pos">Posició</param> 
        /// <returns>Interès quantificat</returns>
        public abstract int Interes(Posicio pos);

        /// <summary> 
        /// Determina si la persona es un convidat (home o dona) o un cambrer 
        /// </summary> 
        /// <returns>Retorna si és convidat</returns>
        public abstract bool EsConvidat();

        /// <summary> 
        /// Retorna el nom de la persona.
        /// </summary> 
        /// <returns>string</returns>
        public override abstract string ToString(); 

    }
}
