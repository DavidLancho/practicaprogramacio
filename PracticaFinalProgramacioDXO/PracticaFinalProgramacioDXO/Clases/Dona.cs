﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{
    public class Dona : Convidat
    {
        private string nom;
        private int sexe;

        /// <summary> 
        /// Crea una Dona 
        /// </summary> 
        /// <param name="nom"> String que identifica aquesta Dona</param> 
        /// <param name="sexe">Plus de simpatia envers convidats del sexe contrari</param> 
        public Dona(string nom, int sexe, TaulaPersones taula)
            : base(nom, sexe, taula)
        {
            this.nom = nom;
            this.sexe = sexe;
        }

        /// <summary> 
        /// Interès d'aquest home per una posició 
        /// </summary> 
        /// <param name="pos">Posició per la qual s'interessa</param> 
        /// <returns>Interès quantificat</returns> 
        public override int Interes(Posicio pos)
        {
            if (!pos.Buida)
            {
                if (!((Persona)pos).EsConvidat())
                {
                    return 0;
                }
                else if (((Convidat)pos).EsHome())
                {
                    return this[(Persona)pos] + PlusSexe;
                }
                else
                {
                    return this[(Persona)pos];
                }
            }
            else return 0;
        }

        /// <summary>
        /// Retorna si és Home.
        /// </summary>
        /// <returns>False</returns>
        public override bool EsHome()
        {
            return false;
        }
        /// <summary>
        /// Retorna el nom.
        /// </summary>
        /// <returns>Nom</returns>
        public override string ToString()
        {
            return this.nom;
        }
    }
}
