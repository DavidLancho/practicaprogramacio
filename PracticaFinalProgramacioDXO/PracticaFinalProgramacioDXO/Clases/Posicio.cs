﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{
    public class Posicio 
    {
        int fil;
        int col;

        /// <summary> 
        /// Crea una nova posició 
        /// </summary> 
        /// <param name="fil">Fila de la Posició</param> 
        /// <param name="col">Columna de la Posició</param> 
        public Posicio(int fil, int col)
        {
            this.fil = fil;
            this.col = col;
        }

        /// <summary> 
        /// Crea una nova posició amb fila i columna igual a 0 
        /// </summary> 
        public Posicio()
        {
            fil = 0;
            col = 0;
        }

        /// <summary> 
        /// Assigna o obté la columna de la posicio 
        /// </summary> 
        public int Columna
        {
            get { return col; }
            set { col = value; }
        }

        /// <summary> 
        /// Assigna o obté la fila de la posicio 
        /// </summary>
        public int Fila
        {
            get { return fil; }
            set { fil = value; }
        }

        /// <summary> 
        /// Retorna si la posició està o no buida 
        /// </summary> 
        public virtual bool Buida
        {
            get { return true; }
            //Retorna true, perquè si hi ha posició és Buida, i si hi ha persona és Plena.
        }

        /// <summary> 
        /// Retorna la distància entre dues posicions 
        /// </summary> 
        /// <param name="pos1">Primera posició</param> 
        /// <param name="pos2">Segona posició</param> 
        /// <returns>Distància entre les dues posicions</returns> 
        public static double Distancia(Posicio pos1, Posicio pos2)
        {
            return Math.Sqrt(Math.Pow(pos2.fil - pos1.fil, 2) + Math.Pow(pos2.col - pos1.col,2));
        }
    }
}
