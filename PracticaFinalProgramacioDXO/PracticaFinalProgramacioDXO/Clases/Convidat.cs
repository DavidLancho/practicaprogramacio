﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{
    public abstract class Convidat : Persona 
    {
        private string nom;
        int sexe;
        Simpatia simpatia;

        public Simpatia Simpatia
        {
            get { return simpatia; }
            set { simpatia = value; }
        }
        public static Random r=new Random();

        /// <summary>
        /// Crea un convidat
        /// </summary>
        public Convidat()
        {
        }
        /// <summary> 
        /// Crea un convidat 
        /// </summary> 
        /// <param name="nom">Caràcter que l'identificarà</param> 
        /// <param name="sexe">Plus de simpatia sobre el sexe contrari</param>
        public Convidat(string nom, int sexe, TaulaPersones taula): base(nom)
        {
            simpatia = new Simpatia();
            if (taula.NumPersones != 0)
            {
                IEnumerator<Persona> i = taula.GetEnumerator();

                while (i.MoveNext())
                {
                    Persona p = i.Current;
                    if (p.EsConvidat())
                    {
                        simpatia.Afegir(p, r.Next(-5, 6));
                        ((Convidat)p).simpatia.Afegir(this, r.Next(-5, 6));
                    }
                }
            }
            this.nom = nom;
            this.sexe = sexe;
        }

        /// <summary> 
        /// Retorna o estableix la simpaties envers a algú 
        /// </summary> 
       public int this [Persona per] 
       {
          get {
              if (nom != per.Nom)
              {
                  return this.simpatia[per];
              }
              else throw new Exception("No es pot buscar la teva propia simpatia");
              }
          set {
              if (nom != per.Nom)
              {
                  this.simpatia[per]=value;
              }
              else throw new Exception("No es pot posar la teva propia simpatia");
          }
       } 

        /// <summary> 
        /// Retorna o estableix el plus de simpatia envers del sexe contrari 
        /// </summary> 
        public int PlusSexe
        {
            get { return sexe; }
            set { sexe = value; }
        }

        /// <summary> 
        /// Retorna que si és un convidat 
        /// </summary> 
        /// <returns>Cert</returns> 
        public override bool EsConvidat()
        {
            return true;
        }
        /// <summary> 
        /// Retorna que si és un home
        /// </summary> 
        public abstract bool EsHome();
        public override abstract string ToString(); 
    }
}
