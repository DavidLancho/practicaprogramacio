﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaFinalProgramacioDXO
{
    public class Simpatia : IEnumerable<int>
    {
        Dictionary<Persona, int> taula;
        /// <summary> 
        /// Crea una taula de referències a persones 
        /// </summary> 
        public Simpatia()
        {
            taula = new Dictionary<Persona, int>();
        }

        /// <summary> 
        /// Assigna o obté una persona de la taula 
        /// </summary> 
        public int this[Persona per]
        {
            get { return taula[per]; }
            set { taula[per] = value; }
        }

        /// <summary> 
        /// Obtè el número total de persones 
        /// </summary> 
        public int NumPersones
        {
            get { return taula.Count; }
        }

        /// <summary> 
        /// Afegeix una persona a la taula 
        /// </su/mmary> 
        /// <param name="conv">Convidat a afegir</param> 
        public void Afegir(Persona pers, int simpatia)
        {
            if (!taula.Keys.Contains(pers)) 
                taula.Add(pers, simpatia);
        }

        /// <summary> 
        /// Eliminar una persona de la taula 
        /// </summary> 
        /// <param name="pers">Persona</param> 
        public void Eliminar(Persona pers)
        {
            taula.Remove(pers);
        }

        public IEnumerator<int> GetEnumerator()
        {
            foreach (KeyValuePair<Persona, int> p in taula)
            {
                yield return p.Value;
            }
        }
        public IEnumerator<Persona> GetEnumerator2()
        {
            foreach (KeyValuePair<Persona, int> p in taula)
            {
                yield return p.Key;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}