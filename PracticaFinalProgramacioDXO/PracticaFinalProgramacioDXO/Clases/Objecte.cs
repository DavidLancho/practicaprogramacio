﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PracticaFinalProgramacioDXO
{
    public class Objecte
    {

        private Label lblPosicio;
        public Objecte(string valorActual, int fila, int columna, Grid tauler)
        {
            lblPosicio = new Label();
            lblPosicio.VerticalContentAlignment = VerticalAlignment.Bottom;
            lblPosicio.HorizontalContentAlignment = HorizontalAlignment.Center;
            lblPosicio.Content = Convert.ToString(valorActual);
            lblPosicio.Foreground = Brushes.White;
            lblPosicio.FontSize = 14;
            lblPosicio.FontWeight = new FontWeight();
            lblPosicio.Padding = new Thickness(-5);
            lblPosicio.SetValue(Grid.ColumnProperty, columna);
            lblPosicio.SetValue(Grid.RowProperty, fila);
            tauler.Children.Add(lblPosicio);
            lblPosicio.HorizontalAlignment = HorizontalAlignment.Center;
            lblPosicio.Margin = new Thickness(2);
        }
        public Label LblN
        {
            get { return lblPosicio; }
            //set { lblNumero.Name = value; }
        }
        public string Valor
        {
            get { return lblPosicio.Content.ToString(); }
            set { lblPosicio.Content = value; }
        }
        public int Columna
        {
            get { return Grid.GetColumn(lblPosicio); }
            set { Grid.SetColumn(lblPosicio, value); }
        }
        public int Fila
        {
            get { return Grid.GetRow(lblPosicio); }
            set { Grid.SetRow(lblPosicio, value); }
        }
    }
}
