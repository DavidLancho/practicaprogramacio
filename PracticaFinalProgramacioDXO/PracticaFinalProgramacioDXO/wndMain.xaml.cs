﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PracticaFinalProgramacioDXO
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class wndMain : Window
    {
        wndTaulerJoc wndTauler;
        public wndMain()
        {
            InitializeComponent();
        }

        private void btnComençar_Click(object sender, RoutedEventArgs e)
        {
            int files = Convert.ToInt32(sldFiles.Value);
            int homes = Convert.ToInt32(sldHomes.Value);
            int dones = Convert.ToInt32(sldDones.Value);
            int cambrers = Convert.ToInt32(sldCambrers.Value);
            if ((cambrers + homes + dones) < (files * files))
            {
                if (wndTauler == null || !wndTauler.IsLoaded)
                {
                    wndTauler = new wndTaulerJoc(files, homes, dones, cambrers);
                    wndTauler.Show();
                }
            }
            else
            {
                MessageBox.Show("Tens que posar mes files hi ha masa gent a la festa.");
            }
        }

        private void sldFiles_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblFiles.Content = sldFiles.Value;
        }

        private void sldHomes_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblHomes.Content = sldHomes.Value;
        }

        private void sldDones_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblDones.Content = sldDones.Value;
        }

        private void sldCambrers_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblCambrers.Content = sldCambrers.Value;
        }
    }
}
