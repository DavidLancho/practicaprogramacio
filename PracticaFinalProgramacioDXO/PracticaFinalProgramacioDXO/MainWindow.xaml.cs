﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PracticaFinalProgramacioDXO
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const Random r = new Random();

        public MainWindow()
        {
            InitializeComponent();

            IniciarEscenari();
        }

        public void IniciarEscenari()
        {
            Escenari escenariFesta = new Escenari(files, columnes);

            for (int i = 0; i < files; i++)
            {
                for (int j = 0; j < columnes; j++)
                {
                    escenariFesta[i, j] = new Posicio();
                }
            }
        }
        public void IniciarPersones()
        {

        }
    }
}
