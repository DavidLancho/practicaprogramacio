﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PracticaFinalProgramacioDXO
{
    /// <summary>
    /// Lógica de interacción para wndTaulaSimpaties.xaml
    /// </summary>
    public partial class wndTaulaSimpaties : Window
    {
        Control[,] estructura;
        Simpatia taula;
        TaulaPersones persones;
        Persona personaActual;
        const int COLUMNA = 4;
        public wndTaulaSimpaties(Simpatia taula, Persona per,TaulaPersones persones)
        {
            InitializeComponent();
            this.taula=taula;
            this.persones = persones;
            this.personaActual=per;
            if (taula.NumPersones != 0)
            {
                mostrar();
                this.Title ="Taula simpatia: "+ personaActual.Nom.ToString();
            }
            

        }
        public void mostrar()
        {
            int i, j;
            int files = taula.Count() + 1;
            estructura = new Control[files, COLUMNA];
            for (i = 0; i < files; i++)
            {
                grdTaula.RowDefinitions.Add(new RowDefinition());
            }
            for (i = 0; i < COLUMNA; i++)
            {
                grdTaula.ColumnDefinitions.Add(new ColumnDefinition());
            }
            IEnumerator<int> sim = taula.GetEnumerator();
            IEnumerator<Persona> per = taula.GetEnumerator2();
            for (i = 0; i < files; i++)
            {
                for (j = 0; j < COLUMNA; j++)
                {
                    if (i == 0)
                    {
                        if (j == 2)
                        {
                            estructura[i, j + 1] = new Label();
                            estructura[i, j + 1].BorderBrush = Brushes.Black;
                            estructura[i, j + 1].BorderThickness = new Thickness(1, 1, 1, 1);
                            estructura[i, j + 1].SetValue(Grid.ColumnProperty, j+1);
                            estructura[i, j + 1].SetValue(Grid.RowProperty, i);
                            estructura[i, j + 1].VerticalContentAlignment = VerticalAlignment.Center;
                            estructura[i, j + 1].HorizontalContentAlignment = HorizontalAlignment.Center;
                            grdTaula.Children.Add(estructura[i, j+1]);
                            ((Label)estructura[i, j + 1]).Content = "Sexe";
                        }
                        else
                        {
                            estructura[i, j] = new Label();
                            estructura[i, j].BorderBrush = Brushes.Black;
                            estructura[i, j].BorderThickness = new Thickness(1, 1, 1, 1);
                            estructura[i, j].SetValue(Grid.ColumnProperty, j);
                            estructura[i, j].SetValue(Grid.RowProperty, i);
                            estructura[i, j].VerticalContentAlignment = VerticalAlignment.Center;
                            estructura[i, j].HorizontalContentAlignment = HorizontalAlignment.Center;
                            grdTaula.Children.Add(estructura[i, j]);

                            if (j == 0)
                            {
                                ((Label)estructura[i, j]).Content = "Jugador";
                            }
                            if (j == 1)
                            {
                                ((Label)estructura[i, j]).Content = "Simpatia";
                            }
                        }


                    }
                    else
                    {
                       
                        if (j == 2 && i == 1)
                        {
                            estructura[i, j + 1] = new TextBox();
                            ((TextBox)estructura[i, j + 1]).Text = ((Convidat)personaActual).PlusSexe.ToString();
                            ((TextBox)estructura[i, j + 1]).TextChanged += Textbox_ChangedContent;
                            estructura[i, j + 1].Tag = ((TextBox)estructura[i, j+1]).Text;
                            estructura[i, j + 1].BorderBrush = Brushes.Black;
                            estructura[i, j + 1].BorderThickness = new Thickness(1, 1, 1, 1);
                            estructura[i, j + 1].SetValue(Grid.ColumnProperty, j+1);
                            estructura[i, j + 1].SetValue(Grid.RowProperty, i);
                            estructura[i, j + 1].VerticalContentAlignment = VerticalAlignment.Center;
                            estructura[i, j + 1].HorizontalContentAlignment = HorizontalAlignment.Center;
                            grdTaula.Children.Add(estructura[i, j + 1]);
                        }
                        else
                        { 
                           

                            if (j == 0)
                            {
                                sim.MoveNext();
                                per.MoveNext();
                                estructura[i, j] = new Label();
                                ((Label)estructura[i, j]).Content = per.Current.Nom.ToString();
                                estructura[i, j].BorderBrush = Brushes.Black;
                                estructura[i, j].BorderThickness = new Thickness(1, 1, 1, 1);
                                estructura[i, j].SetValue(Grid.ColumnProperty, j);
                                estructura[i, j].SetValue(Grid.RowProperty, i);
                                estructura[i, j].VerticalContentAlignment = VerticalAlignment.Center;
                                estructura[i, j].HorizontalContentAlignment = HorizontalAlignment.Center;
                                grdTaula.Children.Add(estructura[i, j]);
                            }
                            else if (j == 1)
                            {
                                estructura[i, j] = new TextBox();
                                ((TextBox)estructura[i, j]).Text = sim.Current.ToString();
                                ((TextBox)estructura[i, j]).TextChanged += Textbox_ChangedContent;
                                estructura[i, j].Tag = ((TextBox)estructura[i, j]).Text;
                                estructura[i, j].BorderBrush = Brushes.Black;
                                estructura[i, j].BorderThickness = new Thickness(1, 1, 1, 1);
                                estructura[i, j].SetValue(Grid.ColumnProperty, j);
                                estructura[i, j].SetValue(Grid.RowProperty, i);
                                estructura[i, j].VerticalContentAlignment = VerticalAlignment.Center;
                                estructura[i, j].HorizontalContentAlignment = HorizontalAlignment.Center;
                                grdTaula.Children.Add(estructura[i, j]);
                            }
                        }
                    }
                }
            }
        }
        private void Textbox_ChangedContent(object sender, TextChangedEventArgs e)
        {
            int resultat;
            if (int.TryParse(((TextBox)e.Source).Text.ToString(), out resultat) && resultat < 6 && resultat>-6)
            {
                int row = Grid.GetRow(e.Source as TextBox);
                int column = Grid.GetColumn(e.Source as TextBox);
                if(column==1)
                {
                    ((TextBox)sender).Tag = ((TextBox)sender).Text;
                    taula[persones[((Label)estructura[row, 0]).Content.ToString()]] = resultat;
                }
                else if(column==3)
                {
                    if (resultat >= 0 && resultat <= 3)
                    {
                        ((TextBox)sender).Tag = ((TextBox)sender).Text;
                        ((Convidat)personaActual).PlusSexe = resultat;
                    }
                    else
                    {
                        ((TextBox)sender).Text = ((TextBox)sender).Tag.ToString();
                    }
                }

            }
            else
            {
               ((TextBox)sender).Text = ((TextBox)sender).Tag.ToString();
            }

        }

    }
}
