﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PracticaFinalProgramacioDXO
{
    /// <summary>
    /// Lógica de interacción para wndTaulerJoc.xaml
    /// </summary>
    public partial class wndTaulerJoc : Window
    {
        string[,] nomsPerPersones;
        public Objecte[,] estructura;
        wndTaulaSimpaties[,] taules;
        Finestres finestresActives;
        public Escenari escenari;
        public int files;
        public int columnes;
        public static Random r = new Random();

        public wndTaulerJoc(int files, int homes, int dones, int cambrers)
        {
            InitializeComponent();
            this.files = files;
            this.columnes = files;
            GuardarNoms();
            escenari = new Escenari(files, files);
            taules = new wndTaulaSimpaties[files, files];
            finestresActives = new Finestres();
            inicialitzarTaulaPersona(homes, dones, cambrers);
            dissenyEscenari();
            Finestres();

        }

        public void dissenyEscenari()
        {
            int i, j;
            for (i = 0; i < files; i++)
            {
                grdTauler.RowDefinitions.Add(new RowDefinition());
            }
            for (i = 0; i < columnes; i++)
            {
                grdTauler.ColumnDefinitions.Add(new ColumnDefinition());
            }
            estructura = new Objecte[files, columnes];
            for (i = 0; i < files; i++)
            {
                for (j = 0; j < columnes; j++)
                {
                    if (!escenari[i, j].Buida)
                    {
                        if (!((Persona)escenari[i, j]).EsConvidat())
                        {
                            estructura[i, j] = new Objecte(((Cambrer)escenari[i, j]).Nom.ToString(), i, j, grdTauler);
                            estructura[i, j].LblN.Tag = "C";
                            estructura[i, j].LblN.Background = new ImageBrush
                            {
                                ImageSource =
                                  new BitmapImage(
                                    new Uri(@"..\..\Imatges\cambrer.png", UriKind.Relative)
                                  )
                            };

                        }
                        else if (((Convidat)escenari[i, j]).EsHome())
                        {
                            estructura[i, j] = new Objecte(((Home)escenari[i, j]).Nom.ToString(), i, j, grdTauler);
                            estructura[i, j].LblN.Tag = "";
                            estructura[i, j].LblN.Background = new ImageBrush
                                {
                                    ImageSource =
                                      new BitmapImage(
                                        new Uri(@"..\..\Imatges\home.png", UriKind.Relative)
                                      )
                                };

                        }
                        else
                        {
                            estructura[i, j] = new Objecte(((Dona)escenari[i, j]).Nom.ToString(), i, j, grdTauler);
                            estructura[i, j].LblN.Tag = "";
                            estructura[i, j].LblN.Background = new ImageBrush
                            {
                                ImageSource =
                                  new BitmapImage(
                                    new Uri(@"..\..\Imatges\dona.png", UriKind.Relative)
                                  )
                            };
                        }

                    }
                    else  estructura[i, j] = new Objecte("", i, j, grdTauler);

                }
            }
        }

        public void refrescarEscenari()
        {
            int i, j;
            for (i = 0; i < files; i++)
            {
                for (j = 0; j < columnes; j++)
                {
                    if (!escenari[i, j].Buida)
                    {
                        if (!((Persona)escenari[i, j]).EsConvidat())
                        {

                            estructura[i, j].Valor = ((Cambrer)escenari[i, j]).Nom.ToString();
                            estructura[i, j].LblN.Background = new ImageBrush
                            {
                                ImageSource =
                                  new BitmapImage(
                                    new Uri(@"..\..\Imatges\cambrer.png", UriKind.Relative)
                                  )
                            };
                        }
                        else if (((Convidat)escenari[i, j]).EsHome())
                        {
                            estructura[i, j].Valor = ((Home)escenari[i, j]).Nom.ToString();
                            estructura[i, j].LblN.Background = new ImageBrush
                            {
                                ImageSource =
                                  new BitmapImage(
                                    new Uri(@"..\..\Imatges\home.png", UriKind.Relative)
                                  )
                            };
                        }
                        else
                        {
                            estructura[i, j].Valor = ((Dona)escenari[i, j]).Nom.ToString();
                            estructura[i, j].LblN.Background = new ImageBrush
                            {
                                ImageSource =
                                  new BitmapImage(
                                    new Uri(@"..\..\Imatges\dona.png", UriKind.Relative)
                                  )
                            };
                        }

                    }
                    else
                    {
                        estructura[i, j].Valor = "";
                        estructura[i, j].LblN.Background = null;
                    }
                }
            }
        }

        public void inicialitzarTaulaPersona(int home, int dona, int cambrer)
        {
           for (int i = 0; i < home; i++ )
           {
               escenari.Posar(new Home(nomsPerPersones[0,i], r.Next(4), escenari.Taula));
           }
           for (int i = 0; i < dona; i++)
           {
               escenari.Posar(new Dona(nomsPerPersones[1, i], r.Next(4), escenari.Taula));
           }
           for (int i = 0; i < cambrer; i++)
           {
               escenari.Posar(new Cambrer("Cambrer"+i));
           }
        }

        private void btnCicle_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < files; i++)
            {
                for (int j = 0; j < columnes; j++)
                {
                    if (taules[i, j]!=null && taules[i, j].IsLoaded)
                    {
                        taules[i, j].Close();
                        taules[i, j]=null;

                    }
                }
            }
            escenari.Cicle();
            refrescarEscenari();
        }

        private void grdT_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.Source is Label||e.Source is Viewbox)
            {
                int row=Grid.GetRow(e.Source as Label);
                int column=Grid.GetColumn(e.Source as Label);
                if (!escenari[row, column].Buida && ((Persona)escenari[row, column]).EsConvidat())
                {
                    if(!finestresActives[((Persona)escenari[row, column])].IsLoaded)
                    {
                        finestresActives[((Persona)escenari[row, column])] = new wndTaulaSimpaties(((Convidat)escenari[row, column]).Simpatia, ((Persona)escenari[row, column]), escenari.Taula);
                        finestresActives[((Persona)escenari[row, column])].Show();
                    }
                }
            }
        }
        private void Finestres()
        {
            IEnumerator<Persona> i = escenari.Taula.GetEnumerator();

            while (i.MoveNext())
            {
                Persona p = i.Current;
                if (((Persona)p).EsConvidat())
                {
                    finestresActives.Afegir(new wndTaulaSimpaties(((Convidat)p).Simpatia, p, escenari.Taula), p);
                }
            }
        }



        private void GuardarNoms()
        {
            nomsPerPersones = new string[2, 5];
            nomsPerPersones[0, 0] = "  Joan  ";
            nomsPerPersones[0, 1] = "  Mike  ";
            nomsPerPersones[0, 2] = "  Kike  ";
            nomsPerPersones[0, 3] = "  Pepe  ";
            nomsPerPersones[0, 4] = "  Ramon  ";
            nomsPerPersones[1, 0] = "  Maria  ";
            nomsPerPersones[1, 1] = "  Luna  ";
            nomsPerPersones[1, 2] = "  Jenny  ";
            nomsPerPersones[1, 3] = "  Katy  ";
            nomsPerPersones[1, 4] = "  Lucia  ";
        }
    }
}
